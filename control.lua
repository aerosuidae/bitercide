
script.on_event({defines.events.on_rocket_launched}, function(event)
	if event.rocket.get_item_count("bitercide-aerosol") > 0 then

		local kills = 0
		local silo = event.rocket_silo
		local spos = silo.position
		local force = silo.force
		local surface = silo.surface

		local level = (force.technologies["bitercide-range"].level or 1) - 1
		local radius = 1000 + (level * 100)

		local enemies = surface.find_entities_filtered({
			force = "enemy",
			position = spos,
			radius = radius,
		})

		local rescan = {}

		for _, entity in ipairs(enemies) do
			local epos = entity.position
			kills = kills + 1

			local cpos = { x = math.floor(epos.x/32), y = math.floor(epos.y/32) }
			local charted = force.is_chunk_charted(surface, cpos)
			local visible = charted and force.is_chunk_visible(surface, cpos)

			if visible then
				-- leave a corpse (slower, UPS hit for large number of kills)
				entity.die(force)
			else
				-- just vanish (fast)
				entity.destroy()
			end

			if charted then
				rescan[cpos.x] = rescan[cpos.x] or {}
				rescan[cpos.x][cpos.y] = true
			end
		end

		for cx, yl in pairs(rescan) do
			for cy, _ in pairs(yl) do
				force.chart(surface, {
					left_top = { x = cx*32, y = cy*32 },
					right_bottom = { x = cx*32+32, y = cy*32+32 },
				})
			end
		end

		silo.surface.create_entity({
			name = "flying-text",
			position = spos,
			color = { r=1, g=1, b=1 },
			text = "bitercide: "..radius.."m (kills "..kills..")",
		})
	end
end)
