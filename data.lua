data:extend({
	{
		type = "technology",
		name = "bitercide",
		icon = "__bitercide__/tech.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "bitercide-aerosol" },
		},
		prerequisites = {
			"rocket-silo",
			"atomic-bomb",
		},
		unit = {
			time = 60,
			count_formula = 1000,
			ingredients = {
				{"automation-science-pack", 1},
				{"logistic-science-pack", 1},
				{"chemical-science-pack", 1},
				{"production-science-pack", 1},
				{"utility-science-pack", 1},
				{"space-science-pack", 1}
			},
		},
		order = "a",
	},
	{
		type = "technology",
		name = "bitercide-range",
		icon = "__bitercide__/tech.png",
		icon_size = 128,
		effects = {},
		prerequisites = {
			"bitercide",
		},
		unit = {
			time = 60,
			count_formula = "1000*L+1000",
			ingredients = {
				{"automation-science-pack", 1},
				{"logistic-science-pack", 1},
				{"chemical-science-pack", 1},
				{"production-science-pack", 1},
				{"utility-science-pack", 1},
				{"space-science-pack", 1}
			},
		},
		max_level = "infinite",
		order = "b",
	},
	{
		type = "item",
		name = "bitercide-aerosol",
		icon = "__bitercide__/aerosol.png",
		icon_size = 32,
		subgroup = "capsule",
		order = "z",
		stack_size = 1,
	},
	{
		type = "recipe",
		name = "bitercide-aerosol",
		category = "crafting",
		enabled = false,
		hidden = false,
		icon = "__bitercide__/aerosol.png",
		icon_size = 32,
		subgroup = "capsule",
		order = "z",
		ingredients = {
			{ type = "item", name = "atomic-bomb", amount = 1 },
			{ type = "item", name = "poison-capsule", amount = 1000 },
		},
		results = {
			{ type = "item", name = "bitercide-aerosol", amount = 1 },
		},
		energy_required = 60,
	}
})